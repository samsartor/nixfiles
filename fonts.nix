{ config, pkgs, ... }:
{
  fonts.packages = with pkgs; [
    iosevka-bin
    (iosevka-bin.override { variant = "sgr-iosevka-term"; })
    (iosevka-bin.override { variant = "aile"; })
    noto-fonts
    noto-fonts-emoji
    noto-fonts-cjk
    powerline-fonts
    font-awesome
    nerdfonts
  ];

  fonts.fontconfig = {
    enable = true;
    defaultFonts = {
      monospace = ["Iosevka"];
    };
  };
}


