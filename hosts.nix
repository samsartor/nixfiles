{ options, ... }:
let
    range = a: b: if a == b then [a] else [a] ++ range (a + 1) b;
    wmLabHosts = map (i: { name="lab${toString i}"; host="th121-${toString i}.cs.wm.edu"; }) (range 1 24);
    wmBgHosts = map (i: { name="bg${toString i}"; host="bg${toString i}.cs.wm.edu"; }) (range 1 14);
    pieterHosts = [
        { name="phong"; host="128.239.22.158"; }
        { name="cook"; host="128.239.22.157"; }
    ];
    wmHosts = wmLabHosts ++ wmBgHosts ++ pieterHosts;
    start9Hosts = [ { name="buildbox"; host="10.0.150.20"; } ];
    allHosts = wmHosts ++ start9Hosts;
in
{   
    networking.extraHosts = builtins.concatStringsSep "\n" (map ({name, host}: "${host} ${name}") allHosts);
}
