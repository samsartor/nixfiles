{ pkgs, ... }: {
  imports = [
    ./sway.nix
  ];

  # Environment variables
  environment.homeBinInPath = true;
  
  # Flatpak
  services.flatpak.enable = true;
  
  # Android debugging
  programs.adb.enable = true;
  
  # Minimal package set to install on all machines.
  environment.systemPackages = with pkgs; [
    clang
    direnv
    eza
    fd
    gcc
    git
    gnupg
    htop
    iftop
    inetutils
    lm_sensors
    neovim
    nix-direnv
    openssl
    restic
    ripgrep
    rsync
    sysstat
    tmux
    tree
    unzip
    vim
    zsh
    openvpn # for start9 stuff
    bluez
  ];

  # Automatically start an SSH agent.
  programs.ssh.startAgent = true;

  # Enable SSH X forwarding
  programs.ssh.forwardX11 = true;
  programs.ssh.setXAuthLocation = true;

  # SSH server into the laptop.
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "no";
    };
  };
  
  # Polkit so programs like gparted work.
  security.polkit.enable = true;

  # Pipewire
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };
  
  # Optionaly enable postgres and neo4j for school stuff
  services.postgresql = {
    enable = true;
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 10 ''
      local all all trust
      host all all 127.0.0.1/32 trust
      host all all ::1/128 trust
    '';
    initialScript = pkgs.writeText "backend-initScript" ''
      CREATE ROLE sam WITH LOGIN CREATEDB;
    '';
  };
  services.neo4j = {
    enable = false;
    shell.enable = true;
    extraServerConfig = ''
    dbms.security.auth_enabled=false
    dbms.default_listen_address=127.0.0.1
    '';
  };
  virtualisation.docker.enable = true;
  
  # Enable ZSH for the command-not-found functionality
  programs.zsh.enable = true;
  environment.pathsToLink = [ "/share/zsh" ];

  # virtual machines
  virtualisation.libvirtd.enable = true;
}
