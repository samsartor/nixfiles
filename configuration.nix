# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./programs
      ./fonts.nix
      ./hosts.nix
    ];
  
  # System options
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
  
  # Bootloader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot/efi";
    };
    
    initrd.systemd.enable = true; # start systemd early
    plymouth.enable = true; # splash screen
    plymouth.theme = "breeze";
    
    kernelParams = [
      "mem_sleep_default=deep" # Fix kohaku suspend
      "boot.shell_on_fail"
      "nosgx" # My kohaku's bios does not enable SGX
      "quiet" "splash" "vt.global_cursor_default=0" # quiet boot
    ];
  };

  # Virtual camera
  boot = {
    extraModulePackages = with config.boot.kernelPackages; [
      v4l2loopback
    ];
    extraModprobeConfig = ''
      # exclusive_caps: Skype, Zoom, Teams etc. will only show device when actually streaming
      # card_label: Name of virtual camera, how it'll show up in Skype, Zoom, Teams
      # https://github.com/umlaeute/v4l2loopback
      options v4l2loopback devices=1 video_nr=1 exclusive_caps=1 card_label="Virtual Camera"
    '';
  };
  
  # Extra filesystems
  boot.supportedFilesystems = [ "ntfs" ];

  # Power button press
  services.logind.extraConfig = ''
    HandlePowerKey=suspend
  '';
  
  # Power management
  services.tlp.enable = true;
  services.tlp.settings = {
    USB_AUTOSUSPEND = "0";
  };
  powerManagement.powertop.enable = false;
  services.power-profiles-daemon.enable = false;

  # Define your hostname.
  networking.hostName = "bonk";
  
  # Firewall
  networking.firewall.enable = false;
  
  # Enable networking
  networking.networkmanager.enable = true;

  # Wireguard for alanet and stuff
  services.bird2.enable = true;
  services.bird2.config = ''
    # ==== CONSTANTS ====

    define OWNAS = 4206905432; # your autonomous system number

    # the first non-zero IPv4 you control. Since we're only using
    # IPv6. Use the first non-zero 32 bits you control. E.g., if
    # you control 255::/16, use 2.85.0.1 (255 is two hex bytes,
    # 0x02, and 0x55 which is 2.85 in a v4 addr).
    define OWNIP = 2.66.0.1;

    # the router's actual IPv6 addr that it can be reached by.
    define OWNIPv6 = 242::1;

    # the subnet you control
    define OWNNETv6 = 242::/16;

    # set of all addrs you control.
    define OWNNETSETv6 = [242::/16+];

    # ===================

    router id OWNIP;

    # "everything" is a protocol in bird. The `device` protocol is not really
    # a protocol, it just instructs bird to ask the kernel for information
    # on devices (interfaces).
    protocol device {
      # scan devices every 10 seconds.
      scan time 10;
    }

    # ==== UTILITY FUNCTIONS ====

    function is_self_net() {
      return net ~ OWNNETSETv6;
    }

    function is_valid_network() {
      return net ~ [
        0200::/7+
      ];
    }

    # Tell bird how to interact with the kernel (modify routes):
    #
    # Again, not really a protocol but ¯\_(ツ)_/¯
    protocol kernel {
      # scan the routing table every 20 seconds (update bird state).
      scan time 20;

      # configure ipv6
      ipv6 {
        # Don't import any routes from the kernel, let bird figure out
        # what routes to add or change. `none` is a "filter" that discards
        # routes that are given to it. The kernel gives bird routes and we
        # want bird to ignore them. kernel -> bird.
        import none;

        # We write an inline filter that describes what routes to give the
        # kernel from bird. bird -> kernel.
        export filter {
          # if the source of this route is static configuration, then we
          # don't want to give it to the kernel. (only want dynamic routes).
          # I don't know why this is important? Is that right?
          if source = RTS_STATIC then reject;
          # set the `krt_prefsrc` attribute for this route.
          # see comment above above the kernel block.
          krt_prefsrc = OWNIPv6;
          # accept this route (don't filter it out).
          accept;
        };
      };
    }

    # Static protocol lets you define static routes that do not change.
    protocol static {
      # reject any routes in our subnet (I think?)
      route OWNNETv6 reject;

      ipv6 {
        # tell bird about everything. static -> bird.
        import all;
        # bird can't configure static routes (default)?.
        # bird -> static.
        export none;
      };
    }

    # Create a template for the `bgp` protocol. This is where the real
    # magic happens. We use a template since every peer will essentially
    # have the same BGP configuration. The template name is `alapeers`.
    #
    # Each peer gets its own template and file under `peers/*``.
    #
    # We're using BGP in "exterior" or `eBGP` mode. This means we're using
    # the protocol to define routes between Autonomous Systems, not really
    # between individual IP addrs.
    #
    # Each instance of the `bgp` protocol corresponds with one neighboring
    # router, or in our case, a peer.
    template bgp alapeers {
      # our `as` number is the OWNAS constant. This is defined in the local
      # defs file that comes later.
      local as OWNAS;
      # compare path lengths to determine which route is the best one.
      path metric 1;

      ipv6 {
        # only accept routes from peers that follow this inline filter.
        # bgp -> bird.
        import filter {
          # Accept the route if its a valid network and is not our
          # own network. (fns defined in local defs file).
          if is_valid_network() && !is_self_net() then {
            accept;
          }
          reject;
        };
        # only send routes that follow this inline filter.
        # bird -> bgp (other peers).
        export filter {
          # Only send if it's a valid netowrk and the route comes from
          # static configuration or from BGP.
          if is_valid_network() && source ~ [RTS_STATIC, RTS_BGP] then {
            accept;
          }
          reject;
        };
        # Only allow for 1000 total routes to be imported. Once we hit
        # 1000, block further routes.
        import limit 1000 action block;
      };
    }

    # Include all of our peers!
    protocol bgp robby from alapeers {
      neighbor 233::1 as 4206944444;
    }
  '';
  networking.wireguard.enable = true;
  networking.wg-quick.interfaces = {
    alanet-robby = {
      listenPort = 51997;
      table = "off";
      peers = [
        {
          endpoint = "76.25.182.86:6484";
          allowedIPs = [ "0200::/7" ];
          persistentKeepalive = 25;
          publicKey = "0G5FkqlyKMDN6xXa5z/N7P2dGbQ2gVZFaVUye1OKeyU=";
        }
      ];
      postUp = ''${pkgs.iproute2}/bin/ip addr add dev alanet-robby 242::1/128 peer 233::1/128'';
      privateKeyFile = "/home/sam/.config/alanet-wg/peer_id";
    };
  };

  # Tor SOCKS5 proxy
  services.tor.enable = true;
  services.tor.client.enable = true;

  # networking.wg-quick.interfaces = {
  #   start9-vpn = {
  #     address = [ "10.0.75.15/32" ];
  #     peers = [
  #       {
  #         endpoint = "96.81.42.77:51820";
  #         allowedIPs = [ "10.0.100.0/24" "10.0.150.0/24" ];
  #         publicKey = "GsuSIisRByekBJhTnTYY/aiH8PdKty4FtXQu95/tnCo=";
  #         presharedKey = "CZfJl2DfoqnctIGfrttfWqUeliKm65K+WVepJo2cY60=";
  #       }
  #     ];
  #     privateKeyFile = "/home/sam/.config/start9-wg/peer_id";
  #   };
  # };

  # Systemd-resolved for custom dns stuff
  services.resolved.enable = false;
  #services.resolved.extraConfig = ''
  #[Resolve]
  #DNS=233:233::10
  #Domains=~ohea
  #'';

  # Enable Avahi for *.local dns discovery.
  services.avahi.enable = true;
  services.avahi.nssmdns = true;
  services.avahi.openFirewall = true;

  # Start9 Root Cert
  security.pki.certificateFiles = [ "/home/sam/Documents/serious-medics.local.crt" ];

  # Set your time zone.
  # time.timeZone = "Australia/Sydney";
  time.timeZone = "America/Denver";
  # time.timeZone = "America/Chicago";
  # time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Make sure opengl is correctly enabled.
  hardware.opengl = {
    enable = true;
    driSupport = true;
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.displayManager.gdm.wayland = true;
  
  # Orientation and ambient light
  hardware.sensor.iio.enable = true;
  
  # Get sound working
  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;

  # Some bluetooth stuff
  hardware.bluetooth.enable = true;
  
  # Fix some wine shit
  hardware.opengl.driSupport32Bit = true;
  
  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  # We can't enable because it causes a conflict with the xdg-desktop-portal-gdk config
  # services.xserver.desktopManager.gnome.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "3l-cros";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.sam = {
    isNormalUser = true;
    description = "Sam Sartor";
    extraGroups = [ "networkmanager" "wheel" "docker" "libvirtd" ];
    shell = pkgs.zsh;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Intel's libva driver
  hardware.opengl.extraPackages = [
    pkgs.intel-media-driver
    pkgs.vaapiVdpau
    pkgs.libvdpau-va-gl
    # pkgs.intel-ocl
  ];
  environment.systemPackages = [ pkgs.libva-utils ];
  
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}
